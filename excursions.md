# Detailed descriptions of the excursion options

## Saturday options

##### French Quarter Walking Tour

Enjoy the beauty and mystique of the French Quarter with a tour. You will view examples of eighteenth and nineteenth century French and Spanish architecture, as well as the wrought iron fences of Jackson Square and a wide array of emporiums and internationally renowned restaurants. 
Your tour will include entrance to the Louisiana State Museum's buildings such as the magnificent Cabildo, which was once used as the New Orleans City Hall. 

##### Pontoon swamp tour

Explore Louisiana in its natural state with a tour through our swamp lands. 
Native Americans and Louisiana Cajuns have been living in harmony with these marshlands for centuries, home to one of the richest and most diverse ecosystems in the world. 
The authentic Cajun guides will escort the group on an expedition through the winding  waterways beneath a canopy of ancient cypress trees dripping with Spanish moss. 
Learn the history of the Cajun people and encounter the unsurpassed beauty of the plants and critters indigenous to the bayous of Louisiana from the comfort of a pontoon boat.
You’re likely to see alligators, bald eagles, herons, egrets, raccoons, deer, black bears, nutria, turtles, ducks and many species of water snakes. 

##### French Quarter Ghost Tour

Spectres of the New Orleans' long and storied past are believed to dwell in the French Quarter. 
Learn the history of America's most haunted city, with our French Quarter Ghost Tour. 
Walk the haunted streets of the French Quarter, as the guide  tells tales of Voodoo, vampires, true crime and the ghosts they created for an eerily-fun adventure. 

##### Pirates Walking Tour

Pirates with their moral compass due south were frequent residents of New Orleans. The French Quarter was their favorite adult playground. 
New Orleans has had a very long love-hate relationship with pirates. Some have been put on trial and hanged while others became heroes and the namesake of many New Orleans landmarks. 
In this tour you will meet the pirates themselves and hear the real history of pirates in New Orleans. Learn the history of Pirates Alley, the pirate brothers Jean and Pierre Laffite, the importance of pirate involvement in the Battle of New Orleans, and so much more. 

##### New Orleans Cooking School

New Orleans is known for its iconic cuisine, and the New Orleans Cooking School will teach your group the basics of Louisiana cooking sure to please any palate. 
You will visit a one of a kind cooking school in a renovated molasses warehouse built in the early 1800s in the heart of the French Quarter. 
Creole/Cajun experts will teach New Orleans specialties such as Gumbo, Jambalaya and Pralines, seasoned with history, trivia and tall tales. 
The group will take home written recipes of these classic delicacies, so they can recreate a taste of New Orleans wherever they are. 

##### History of the Cocktail Walking Tour

New Orleans is home to the cocktail and our History of the Cocktail Walking Tour will introduce the group to the historic locations where these legendary concoctions were born. 
Interactive presentations that use famous cocktails and ingredients tell the rich history of New Orleans and its notable characters. 
The cocktail historian will guide the group through unique locations throughout the French Quarter and regale you with tales of rum, rebellion, whiskey, prohibition and more. 
The tour includes a sampling of several famous New Orleans cocktails and punches.

Includes Four (4) 4oz Sample Cocktails 
* French 75
* Sazerac
* Hurricane
* Grass Hopper

##### Oak Alley Plantation

South Louisiana is known for its beautiful antebellum architecture, and there are plenty of historic plantation homes within an hour's drive from New Orleans. 
We'll take you on a tour of the antebellum south and learn the dynamic and nuanced history of this bygone era.
Feel the gentle breeze of southern hospitality as you travel down the River Road, as we take you back to a time of mint juleps and gracious living. 
Following this view into early plantation life, we will head to Oak Alley Plantation which has been called the "Grande Dame of the Great River Road". The quarter-mile canopy of giant live oak trees, believed to be nearly 300 years old, forms an impressive avenue leading to this classic Greek-revival style antebellum home. As you tour the house and grounds of Oak Alley, 
you will feel as though you were transported back to those days of Southern elegance and charm, while exploring all facets of the plantation past with The Slavery at Oak Alley and Civil War exhibits.

##### Cemetery Walking Tour

Visiting either Lafayette or St. Louis Cemetery

* Lafayette Cemetery # 1 

is New Orleans’ oldest city-operated burial ground. Established in 1833; the bricked walkways of this ‘city of the dead’ are very picturesque. 
The area was known as the City of Lafayette at the time and the gothic-style vaults and tombs are surrounded by the enchanting Garden District as it is known now. 
The notable citizens of the post-Louisiana Purchase era are evident in the family names in the marbles as in the Americanization of the area, including the Baird family name of our owner Meg Baird. 
In recent years, the allure of the stately space has been captured repeatedly by Hollywood South. Anne Rice staged her funeral along the picturesque pathways. The serene area was also showcased in Dracula 2000 and Double Jeopardy. 
Many Hollywood blockbusters were filmed in this area of unique architecture and oak tree lined streets. Notable homes include several of Anne Rice, Trent Reznor, John Goodman, and Sandra Bullock among others. 
Many homes in this area  have been connected with actual documented haunting.

* St. Louis Cemetery # 1 

is the burial ground of the elite blue blood New Orleanians and the oldest existing cemetery. Established in the late 18th century, St. Louis Cemetery # 1 is still active today, and  only accessible to the public through a licensed tour guide. 
This guided tour offers a glimpse into the evolution of death and burial practices in the deep South as we walk through a wide variety of tombs through three centuries.  
The final resting place of Marie Laveau the Voodoo Queen of New Orleans, is located here usually marked by visitors with three XXX’s in red, and Nicolas Cage owns a pyramid-shaped vault here for his future and final reservation. 
Fans of the cult classic film Easy Rider will be interested to see where a particularly sordid scene was filmed, without permission.




## Sunday options

##### French Quarter Walking Tour

Enjoy the beauty and mystique of the French Quarter with a tour. You will view examples of eighteenth and nineteenth century French and Spanish architecture, as well as the wrought iron fences of Jackson Square and a wide array of emporiums and internationally renowned restaurants. 
Your tour will include entrance to the Louisiana State Museum's buildings such as the magnificent Cabildo, which was once used as the New Orleans City Hall. 

##### Pontoon swamp tour

Explore Louisiana in its natural state with a tour through our swamp lands. 
Native Americans and Louisiana Cajuns have been living in harmony with these marshlands for centuries, home to one of the richest and most diverse ecosystems in the world. 
The authentic Cajun guides will escort the group on an expedition through the winding  waterways beneath a canopy of ancient cypress trees dripping with Spanish moss. 
Learn the history of the Cajun people and encounter the unsurpassed beauty of the plants and critters indigenous to the bayous of Louisiana from the comfort of a pontoon boat.
You’re likely to see alligators, bald eagles, herons, egrets, raccoons, deer, black bears, nutria, turtles, ducks and many species of water snakes. 

##### French Quarter Ghost Tour

Spectres of the New Orleans' long and storied past are believed to dwell in the French Quarter. 
Learn the history of America's most haunted city, with our French Quarter Ghost Tour. 
Walk the haunted streets of the French Quarter, as the guide  tells tales of Voodoo, vampires, true crime and the ghosts they created for an eerily-fun adventure. 

##### Oak Alley Plantation

South Louisiana is known for its beautiful antebellum architecture, and there are plenty of historic plantation homes within an hour's drive from New Orleans. 
We'll take you on a tour of the antebellum south and learn the dynamic and nuanced history of this bygone era.
Feel the gentle breeze of southern hospitality as you travel down the River Road, as we take you back to a time of mint juleps and gracious living. 
Following this view into early plantation life, we will head to Oak Alley Plantation which has been called the "Grande Dame of the Great River Road". The quarter-mile canopy of giant live oak trees, believed to be nearly 300 years old, forms an impressive avenue leading to this classic Greek-revival style antebellum home. As you tour the house and grounds of Oak Alley, 
you will feel as though you were transported back to those days of Southern elegance and charm, while exploring all facets of the plantation past with The Slavery at Oak Alley and Civil War exhibits.

##### Cemetery Walking Tour

Visiting either Lafayette or St. Louis Cemetery

* Lafayette Cemetery # 1 

is New Orleans’ oldest city-operated burial ground. Established in 1833; the bricked walkways of this ‘city of the dead’ are very picturesque. 
The area was known as the City of Lafayette at the time and the gothic-style vaults and tombs are surrounded by the enchanting Garden District as it is known now. 
The notable citizens of the post-Louisiana Purchase era are evident in the family names in the marbles as in the Americanization of the area, including the Baird family name of our owner Meg Baird. 
In recent years, the allure of the stately space has been captured repeatedly by Hollywood South. Anne Rice staged her funeral along the picturesque pathways. The serene area was also showcased in Dracula 2000 and Double Jeopardy. 
Many Hollywood blockbusters were filmed in this area of unique architecture and oak tree lined streets. Notable homes include several of Anne Rice, Trent Reznor, John Goodman, and Sandra Bullock among others. 
Many homes in this area  have been connected with actual documented haunting.

* St. Louis Cemetery # 1 

is the burial ground of the elite blue blood New Orleanians and the oldest existing cemetery. Established in the late 18th century, St. Louis Cemetery # 1 is still active today, and  only accessible to the public through a licensed tour guide. 
This guided tour offers a glimpse into the evolution of death and burial practices in the deep South as we walk through a wide variety of tombs through three centuries.  
The final resting place of Marie Laveau the Voodoo Queen of New Orleans, is located here usually marked by visitors with three XXX’s in red, and Nicolas Cage owns a pyramid-shaped vault here for his future and final reservation. 
Fans of the cult classic film Easy Rider will be interested to see where a particularly sordid scene was filmed, without permission.


##### Garden District Tour

We'll take you on a driving tour of one of New Orleans' most exclusive neighborhoods, the Garden District. Known for its tapestry of architectural styles and lavish gardens, this stately neighborhood is considered one of the best-preserved collections of historic mansions in the Southern United States. 
Dating back to 1832, this neighborhood was originally developed by wealthy Americans flooding the area after the Louisiana Purchase, eschewing the French Creole gentry of the French Quarter. 
Experience the splendor of the live oak tree-lined "American sector" of town and examine lush gardens and exquisite examples of architectural styles including Greek Revival, Italianate, Gothic, Georgian, Swiss Chalet, Queen Anne and more. 
The tour includes a stop at the historic Elms Mansion, a stunning example of Italianate style architecture built in 1869, complemented with elegant period furnishings and surrounded by lovely gardens and patios. 

